/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import Tts from 'react-native-tts';
import Voice from 'react-native-voice';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    Voice.onSpeechStart = this.onSpeechStartHandler.bind(this);
    Voice.onSpeechEnd = this.onSpeechEndHandler.bind(this);
    Voice.onSpeechPartialResults = this.onSpeechPartialResultsHandler.bind(
      this,
    );
    Voice.onSpeechResults = this.onSpeechResultsHandler.bind(this);
    Voice.onSpeechError = this.onSpeechErrorHandler.bind(this);
    Tts.setDefaultLanguage('tr_TR');
    this.state = {
      text: '',
    };
  }
  onPressSpeech = () => {
    Tts.stop();
    Tts.speak(this.state.text);
  };

  componentWillUnmount() {
    // Remove all listeners
    Voice.removeAllListeners();
  }

  onSpeechStartHandler() {
    console.log('Speech started');
    // Update state to notify user that speech recognition has started
  }

  onSpeechPartialResultsHandler(e) {
    // e = { value: string[] }
    // Loop through e.value for speech transcription results
    console.log('Partial results', e.value[0]);
    const text = e.value[0];
    this.setState({text: text});
  }

  onSpeechResultsHandler(e) {
    // e = { value: string[] }
    // Loop through e.value for speech transcription results
    console.log('Speech results', e.value[0]);
  }

  onSpeechEndHandler(e) {
    // e = { error?: boolean }
    console.log('Speech ended', e);
  }

  onSpeechErrorHandler(e) {
    // e = { code?: string, message?: string }
    //switch (e.code){'...'}
  }

  onStartButtonPress = async () => {
    try {
      await Voice.start('tr_TR');
    } catch (exception) {
      // exception = Error | { code: string, message?: string }
      //onSpeechErrorHandler(exception);
    }
  };
  onStopSpeech = async () => {
    try {
      await Voice.stop();
    } catch (exception) {
      // exception = Error | { code: string, message?: string }
      //onSpeechErrorHandler(exception);
    }
  };
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Text>Hello</Text>
        <TouchableOpacity onPress={this.onPressSpeech}>
          <Text>Konuştur</Text>
        </TouchableOpacity>
        <TextInput
          style={{
            borderBottomColor: 'red',
            borderWidth: 1,
            width: 400,
          }}
          onPress={this.onPressSpeech}
          value={this.state.text}
        />
        <Text>{this.state.text}</Text>
        <TouchableOpacity onPress={this.onStartButtonPress}>
          <View>
            <Text>Start</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.onStopSpeech}>
          <View>
            <Text>Stop</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  }
}
